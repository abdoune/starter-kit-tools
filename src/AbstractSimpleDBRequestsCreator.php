<?php

namespace PwStarterKit\Tools;

/**
 * Class AbstractSimpleDB
 */
abstract class AbstractSimpleDBRequestsCreator
{

    protected $bd;

    /**
     * AbstractAdmin constructor.
     */
    public function __construct()
    {
        global $bd;
        $this->bd = $bd;
    }

    protected function find(int $id, string $table)
    {
        $result = $this->findBy($params, 1, $table);

        if(!empty($result)) {
            return array_shift($result);
        }

        return false;
    }

    /**
     * Find row by field name and value, $params = ["field" => value], get One result by param, table
     *
     * @param array $params
     * @param string $table
     * @return mixed
     * @throws Exception
     */
    protected function findOneBy(array $params, string $table, array $order = [])
    {
        $result = $this->findBy($params, 1, $table);

        if(!empty($result)) {
            return array_shift($result);
        }

        return false;
    }

    /**
     * Find all elements of a $table
     *
     * @param string $table
     * @param int $limit
     * @return array
     */
    protected function findAll(string $table, int $limit , array $order = [])
    {
        $rq = "SELECT * FROM " .$table;

        if(!empty($order)){
            $i = 0;
            foreach ($order as $by => $val){
                if($i == 0) {
                    $rq .= " ORDER BY ".$by." ".$val;
                }else{
                    $rq .= ", ".$by." " .$val;
                }
                $i++;
            }
        }

        $rq .= " LIMIT ".$limit;

        $sql = $this->bd->prepare($rq);

        $sql->execute();

        return $sql->fetchAll();
    }

    /**
     * Find multipls row by one param or more
     *
     * @param array $params
     * @param int|null $limit
     * @param string $table
     * @return array
     * @throws Exception
     */
    protected function findBy(array $params,int $limit ,string $table, array $order = [])
    {
        $fields = array_keys($params);
        $value = array_values($params);


        $checkFields = $this->checkField($fields, $table);
        if(!empty($checkFields)){
            throw new \Exception("Fields ".implode(", ", $checkFields)." not found in table ".$table);
        }


        $sql = "SELECT * FROM " . $table . " WHERE ";

        $constructSQL = $this->constructSQL($fields, $value, $order);
        $sql .= $constructSQL['sql'];
        $sql .= " LIMIT ".$limit;
        $req = $this->bd->prepare($sql);
        $req->execute($constructSQL['values']);

        $results = $req->fetchAll();
        return $results;
    }

    /**
     * Update row by field name and value, $params = ["field" => $value , ...,"id" => $id], id should be the last key in params []
     *
     * @param array $params
     * @param string $table
     * @throws Exception
     */
    protected function updateById(array $params, string $table)
    {
        $sql = "UPDATE ".$table." SET";
        $fields = array_keys($params);
        array_pop($fields);

        foreach ($fields as $field) {
            $sql .= " $field = ? ,";
        }
        $sql = rtrim($sql, ',');
        $sql .= "WHERE id = ? LIMIT 1";
        try {
            $rq = $this->bd->prepare($sql);
            $rq->execute(array_values($params));
        } catch (Exception $e) {
            die('#17 Erreur lors du transfert des données : ' . $e);
        }
    }

    /**
     * Insert data in db , $params = ["field" => $value]
     *
     * @param array $params
     * @param string $table
     * @return string
     */
    protected function set(array $params, string $table)
    {
        $fields = implode(",",array_keys($params));
        $firstValues = array_keys($params);

        $values = [];
        foreach ($firstValues as $value){
            $newValue = ':'.$value;
            array_push($values, $newValue);
        }
        $values = implode(",",$values);

        $sql = "REPLACE INTO $table (".$fields.") VALUES (".$values.")";

        try {
            $rq = $this->bd->prepare($sql);
            $rq->execute($params);
            return $this->bd->lastInsertId();
        } catch (Exception $e) {
            die('#17 Erreur lors du transfert des données : ' . $e);
        }
    }

    /**
     * Delete one element by id
     *
     * @param array $params
     * @param string $table
     * @return string
     */
    protected function deleteById(array $params, string $table)
    {
        $sql = "DELETE FROM $table WHERE id = ?";

        try {
            $rq = $this->bd->prepare($sql);
            $rq->execute(array($params['id']));
            return $this->bd->lastInsertId();
        } catch (Exception $e) {
            die('#17 Erreur lors du transfert des données : ' . $e);
        }
    }

    /**
     * To check if a field exist in  $table
     *
     * @param array $fields
     * @param string $table
     * @return bool
     */
    private function checkField(array $fields, string $table)
    {
        $rows = $this->bd->query("SHOW COLUMNS FROM ".$table);
        $results = $rows->fetchAll();

        foreach ($fields as $key => $fieldSearch) {
            foreach ($results as $result) {
                if($fieldSearch == $result->Field){
                    unset($fields[$key]);
                }
            }
        }

        return $fields;
    }

    /**
     * @param array $fiels
     * @param array $values
     * @param array $order
     * @return void
     */
    private function constructSQL(array $fields, array $values, array $order = [])
    {
        $sql = "";
        $i = 0;
        foreach ($fields as $field) {

            //Adding multiple colonne (created_at, created_at_bis, created_at_ter)
            $multiplie = ['_bis', '_ter', '_ter1', '_ter2'];
            if (str_replace($multiplie, '', $field[$i]) != $field[$i]) {
                $field[$i] = str_replace($multiplie, '', $field[$i]);
            }

            //Gestion premier params
            if ($i == 0) {
                $and = " ";
            } else {
                $and = " AND ";
            }

            //Gestion opérateur findBy(['champs' => 'operateur value']
            $operators = ['<', '<=', '>', '>=', '!=', 'LIKE'];

            //If true c'est qu'il y a un opérateur au dessus ;)
            if (str_replace($operators, '', $values[$i]) != $values[$i]) {
                $explodeOperators = explode(' ', $values[$i]);
                $sql .= " $and " . $fields[$i] . ' ' . $explodeOperators[0] . " ? ";
                $values[$i] = str_replace($operators, '', $values[$i]);
                unset($explodeOperators[0]);
                $values[$i] = implode(' ', $explodeOperators);
            } else {
                //Pas d'opérateur mais possibilité d'avoir is ~not null
                if ($values[$i] == "IS NOT NULL") {
                    $sql .= " $and " . $fields[$i] . " IS NOT NULL ";
                    unset($values[$i]);
                } elseif ($values[$i] == "IS NULL") {
                    $sql .= " $and " . $fields[$i] . " IS NULL ";
                    unset($values[$i]);
                } else {
                    //Sinon requête normal
                    $sql .= " $and " . $fields[$i] . " = ? ";
                }
            }



            $i++;
        }

        if(!empty($order)){
            $i = 0;
            foreach ($order as $by => $val){
                if($i == 0) {
                    $sql .= " ORDER BY ".$by." ". $val;
                }else{
                    $sql .= ", ".$by." ". $val;
                }
                $i++;
            }
        }

        return array('sql' => $sql, 'values' => $values);
    }

}
